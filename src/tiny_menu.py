# MIT License: https://gitlab.com/joeysbytes-python-snippets/tiny-menu/-/blob/main/LICENSE?ref_type=heads
# Copyright (c) 2024 Joey Rockhold

from typing import Sequence, Optional; MENU_COLOR: bool = False


def menu(items: Sequence[str], title: Optional[str] = None, prompt: Optional[str] = "Enter Choice:") -> int:
    def fg(idx:int,text:str)->str:return text if not MENU_COLOR else f"\033[{idx%8+30+(60*(idx>7))}m{text}\033[39m"
    while True:
        print("" if title is None else f"\n{fg(11, title)}\n{fg(11,'-' * len(title))}")
        for idx, item in enumerate(items): print(f'{fg(12, f"{idx+1: >3}")}{fg(14, ")")} {fg(7, item)}')
        try:
            menu_idx = int(input(f"\n{fg(2, '>')} " if prompt is None else f"\n{fg(2, prompt)} ")) - 1
            if not 0 <= menu_idx < len(items): raise ValueError
            print(); return menu_idx
        except Exception:
            print(f'\n{fg(9,"ERROR:")} Invalid Choice, please choose between {fg(13,"1")} and {fg(13,f"{len(items)}")}\n')
