from tiny_menu import menu
import tiny_menu


tiny_menu.MENU_COLOR = True


items = ("Kitten", "Puppy", "Parrot", "Turtle", "Goldfish", "Hamster",
         "Gerbil", "Bald Eagle", "Raccoon", "Squirrel", "Chipmunk", "Fox")

title = "Recipe Chooser"
# title = None

prompt = "What are you in the mood to eat?"
# prompt = None

chosen = menu(items=items, title=title, prompt=prompt)

print(f"You are going to eat: {items[chosen]}")
