# Tiny Menu

Given a sequence of items, prompt the user to choose one, and return the index of the
chosen sequence item.

## Parameters

| Parameter | Default Value | Description                                                |
|-----------|---------------|------------------------------------------------------------|
| items     | _n/a_         | A sequence (list, tuple, etc) of menu items to choose from |
| title     | _None_        | Menu title, optional                                       |
| prompt    | Enter Choice: | The prompt text, optional, outputs '>' if _None_           |

## Variables

| Variable   | Default Value | Description                   |
|------------|---------------|-------------------------------|
| COLOR_MENU | False         | True/False to use ANSI colors |
